﻿using UnityEngine;
using System.Collections;

public class MousePosition : MonoBehaviour {
    
    private Camera cam;

	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public Vector3 getMousePosition()
    {
        return cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.transform.position.y));
    }
}
