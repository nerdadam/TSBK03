﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Agent : MonoBehaviour
{

    public Vector3 target;
    public float speed;
    //public Agent other;
    public float radius;
    public Vector3 velocity;
    public Vector2 newVelocity, prefVelocity;
    public float timeInv;
    public bool selected = false;
    public bool goalReached = false;
    

    private CharacterController cc;

    private Vector3 center;
    private float r;
    private Plane p1, p2;
    private Vector3 tp1, tp2, tp3, tp4, apex, u, temp;
    private List<Line> orcaLines = new List<Line>();
    public List<Agent> agents = new List<Agent>();
    public List<Obstacle> obstacles = new List<Obstacle>();

    private Vector3 b;
    private Line test;
    private Line line;
    private MousePosition mousePosition;
    private GameManager gm;

    // Use this for initialization
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        cc = GetComponent<CharacterController>();
        //radius = GetComponent<SphereCollider>().radius;
        velocity = (target - transform.position).normalized * speed;
        velocity.y = 0;
        //if (asd)
        //    velocity = velocity.normalized;
        newVelocity = new Vector2(velocity.x, velocity.z);
        mousePosition = FindObjectOfType<MousePosition>();
        //agents = gm.agents;
        //agents = FindObjectsOfType<Agent>();
        //target = ((RTSMode) ? 1: -1) * transform.position;
        //obstacles = FindObjectsOfType<Obstacle>();
    }

    // Update is called once per frame
    public void calcVelocity()
    {
        

        //velocity = new Vector3(newVelocity.x, 0, newVelocity.y);

        
        if (goalReached)
        {
            newVelocity = Vector2.zero;
            return;
        }
        
        orcaLines.Clear();
        Vector3 pV = (target - transform.position).normalized * speed;
        //Vector3 pV = (target - transform.position);
        prefVelocity = new Vector2(pV.x, pV.z);
        
        //if (asd)
        //    prefVelocity = prefVelocity.normalized;

        int numObstacles = 0;
        foreach (Obstacle obs in obstacles)
        {
            int counter = 0;
            foreach (Plane p in obs.planes)
            {
                if (!p.GetSide(transform.position))
                {
                    counter++;
                    continue;
                }
                Vector3 point1, point2;
                if (counter == 0)
                {
                    point1 = obs.point1;
                    point2 = obs.point2;
                }
                else if (counter == 1)
                {
                    point1 = obs.point2;
                    point2 = obs.point3;
                }
                else if (counter == 2)
                {
                    point1 = obs.point3;
                    point2 = obs.point4;
                }
                else
                {
                    point1 = obs.point4;
                    point2 = obs.point1;
                }
                Vector3 v = point1 - point2;
                center = v / 2 + point2 - transform.position;
                tp3 = center - v / 2;// + v.normalized*radius;
                tp4 = center + v / 2;// - v.normalized*radius;
                float obstacleTau = 0.3f;
                apex = center / obstacleTau;
                tp1 = tp3 / obstacleTau;
                tp2 = tp4 / obstacleTau;
                //p1 = new Plane(w.normalized, apex + w.normalized*radius);
                p1 = new Plane(Vector3.Cross(Vector3.up, (tp1 - tp2).normalized), apex - (radius * Vector3.Cross(Vector3.up, (tp1 - tp2).normalized)) / obstacleTau);
                p2 = new Plane(v.normalized, Vector3.zero);
                line = new Line();
                if (!p1.GetSide(Vector3.zero) && !p2.SameSide(tp1, tp2))// (tp1.x/tp2.x < 0 || (tp1.z/tp2.z) < 0)) 
                {
                    //Debug.Log("Base "+p.normal);
                    Vector3 dir = (tp1 - tp2).normalized;
                    line.direction = new Vector2(dir.x, dir.z);
                    Vector3 point = apex - radius / obstacleTau * Vector3.Cross(Vector3.up, dir);
                    line.point = new Vector2(point.x, point.z);
                }
                else
                {
                    p2 = new Plane((tp1 - tp2).normalized, apex);
                    if (false)//p2.GetSide(Vector3.zero))
                    {
                        //Debug.Log("Left "+p.normal);
                        /*
                        Vector3 dir = (tp3 - tp1).normalized;
                        line.direction = new Vector2(dir.x, dir.z);
                        Vector3 point = Vector3.Dot(tp1, Vector3.Cross(Vector3.up, dir)) * -Vector3.Cross(Vector3.up, dir);
                        line.point = new Vector2(point.x, point.z);
                        */
                        Vector3 dir = Vector3.Cross(Vector3.up, (-tp1).normalized);
                        line.direction = new Vector2(dir.x, dir.z);
                        //line.point = new Vector2(tp1.x, tp1.z);
                        line.point = vec3tovec2((radius / obstacleTau) * (-tp1).normalized * 1 + tp1);
                    }
                    else
                    {
                        //Debug.Log("Right "+p.normal);
                        /*
                        Vector3 dir = (-tp4 + tp2).normalized;
                        line.direction = new Vector2(dir.x, dir.z);
                        Vector3 point = Vector3.Dot(tp2, Vector3.Cross(Vector3.up, dir)) * -Vector3.Cross(Vector3.up, dir);
                        line.point = new Vector2(point.x, point.z);
                        */
                        Vector3 dir = Vector3.Cross(Vector3.up, (-tp2).normalized);
                        line.direction = new Vector2(dir.x, dir.z);
                        //line.point = new Vector2(tp2.x, tp2.z);
                        line.point = vec3tovec2((radius / obstacleTau) * (-tp2).normalized * 1 + tp2);
                    }
                }
                //Debug.DrawRay(vec3tovec2(line.point), vec3tovec2(line.direction)*100, Color.yellow);
                //Debug.Log(line.point + " " + line.direction);
                orcaLines.Add(line);
                test = line;
                counter++;
                numObstacles++;
            }
        }

        foreach (Agent other in agents)
        {
            Plane plane = new Plane(pV.normalized, transform.position - pV.normalized*radius);
            if (other == null || this == other)// || (other.transform.position - transform.position).magnitude-(radius+other.radius) > (speed+other.speed)*timeInv || !plane.GetSide(other.transform.position))
                continue;
            //center = other.transform.position + other.velocity;
            center = other.transform.position - transform.position;
            r = other.radius + radius;
            //apex = (transform.position + velocity + other.transform.position + other.velocity) / 2;
            //apex = (velocity + other.velocity) / 2;
            apex = center / timeInv;
            //Vector3 axisRay = Vector3.Cross(apex - center, Vector3.up);
            //tp1 = axisRay.normalized * r + center;
            //tp2 = -axisRay.normalized * r + center;
            //findTangents(apex, center, r);
            //findTangents(Vector3.zero, apex, r/timeInv, ref tp1, ref tp2);
            //findTangents(tp1, center, r, ref tp3, ref temp);
            //findTangents(tp2, center, r, ref temp, ref tp4);
            findTangents(Vector3.zero, center, r, ref tp3, ref tp4);
            findTangents(tp3, apex, r/timeInv, ref temp, ref tp1);
            findTangents(tp4, apex, r/timeInv, ref tp2, ref temp);
            //p1 = new Plane(apex, tp1 + new Vector3(0.0f, 1.0f, 0.0f), tp1);
            //p2 = new Plane(apex, tp2, tp2 + new Vector3(0.0f, 1.0f, 0.0f));
            p1 = new Plane(tp1, tp3 + new Vector3(0.0f, 1.0f, 0.0f), tp3);
            p2 = new Plane(tp2, tp4, tp4 + new Vector3(0.0f, 1.0f, 0.0f));
            
            //Vector3 point = transform.position + velocity - other.velocity;
            Vector3 point = velocity - other.velocity;
            //if (Vector3.Dot(transform.position + velocity - other.velocity,p1.normal) < 0 && Vector3.Dot(transform.position + velocity - other.velocity, p2.normal) < 0)
            Vector3 v = point - apex;
            //float d1 = Vector3.Dot(v, -p1.normal);
            //float d2 = Vector3.Dot(v, -p2.normal);
            float d1, d2;
            if (Vector3.Dot(v, p1.normal) > 0)
                d1 = Vector3.Dot(v, p1.normal);
            else
                d1 = Vector3.Dot(v, -p1.normal);
            if (Vector3.Dot(v, p2.normal) > 0)
                d2 = Vector3.Dot(v, p2.normal);
            else
                d2 = Vector3.Dot(v, -p2.normal);

            //if (!p1.GetSide(point) && !p2.GetSide(point))
            //{
            // Debug.Log("Inside velocity object");
            //Debug.Log(d1 + " "+d2);
            line = new Line();
            //Plane p3 = new Plane((center - apex).normalized, apex);
            Plane p3 = new Plane(Vector3.Cross((tp1- tp2).normalized,Vector3.up), tp1);
            if (!p3.GetSide(point) && Mathf.Pow(Vector3.Dot(v, center),2) > Mathf.Pow(r, 2) * Mathf.Pow(v.magnitude, 2))
            //if (Vector3.Dot(v, center) < 0.0f && RVOMath.sqr(Vector3.Dot(v, center)) > Mathf.Pow(r, 2) * Mathf.Pow(v.magnitude, 2))
            {
                //Debug.Log("Circle");
                /*Vector3 cutoff = Vector3.Cross(p3.normal, Vector3.up);
                line.direction = new Vector2(cutoff.x, cutoff.z);
                u = Vector3.Dot(v, -p3.normal) * cutoff - point;
                */
                //v = point - (other.transform.position - transform.position) / 2;
                //Vector3 dir = Vector3.Cross(v.normalized, Vector3.up);
                line.direction = new Vector2(v.normalized.z, -v.normalized.x);
                u = (r / timeInv - v.magnitude) * v.normalized;
            }
            else
            {
                //Debug.Log("Legs");
                Plane p4 = new Plane((tp2 - tp1).normalized, apex);
                //if (d1<d2 && (Vector3.Dot(v,p1.normal) > 0 || (Vector3.Dot(v,p1.normal) < 0 && Vector3.Dot(v, p2.normal) < 0)))
                //if (RVOMath.det(new Vector2(center.x,center.z), new Vector2(v.x,v.z)) < 0.0f)
                if (!p4.GetSide(point))
                {
                    //u = -d1 * -p1.normal - point;
                    //u = d1 * p1.normal;
                    //line.direction = new Vector2(p1.normal.x, p1.normal.z);
                    
                    Vector3 dir = (-tp3+tp1).normalized;
                    if (dir.magnitude == 0)
                        dir = Vector3.Cross(center, Vector3.up);
                    line.direction = new Vector2(dir.x, dir.z);
                    u = d1 * dir - point;
                    
                    /*d1 = Vector3.Dot(point-tp1, -p1.normal);
                    u = d1 * p1.normal - point;
                    v = (tp3 - tp1).normalized;
                    v = Vector3.Cross(v,Vector3.up);
                    line.direction = new Vector2(v.x, v.z);
                    */
                    //float leg = Mathf.Sqrt(Mathf.Pow(center.magnitude,2) - Mathf.Pow(r,2));
                    //line.direction = new Vector2(center.x * leg - center.y * r, center.x * r + center.y * leg) / Mathf.Pow(center.magnitude,2);
                }
                else
                {
                    //u = -d2 * -p2.normal - point;
                    //u = d2 * p2.normal;
                    //line.direction = new Vector2(p2.normal.x, p2.normal.z);

                    Vector3 dir = (tp4 - tp2).normalized;
                    if (dir.magnitude == 0)
                        dir = Vector3.Cross(center, Vector3.up);
                    line.direction = new Vector2(dir.x, dir.z);
                    u = d2 * dir - point;
                    
                    /*d2 = Vector3.Dot(point - tp2, -p2.normal);
                    u = d2 * p2.normal - point;
                    v = (tp4 - tp2).normalized;
                    v = Vector3.Cross(v,Vector3.up);
                    line.direction = new Vector2(v.x, v.z);
                    */
                    //float leg = Mathf.Sqrt(Mathf.Pow(center.magnitude, 2) - Mathf.Pow(r, 2));
                    //line.direction = -new Vector2(center.x * leg + center.y * r, -center.x * r + center.y * leg) / Mathf.Pow(center.magnitude, 2);
                }
                //float dotProduct2 = Vector2.Dot(new Vector2(point.x, point.z), line.direction);
                //u = dotProduct2 * new Vector3(line.direction.x,0,line.direction.y) - point;
            }
            //Debug.Log(p1.normal+" "+d1);
            //Debug.Log(p2.normal + " " + d2);
            //Debug.Log(u);
            //Plane orca = new Plane(u.normalized, velocity + u * 0.5f);
            //Vector3 p = transform.position + velocity + u * 0.5f;
            Vector3 p = velocity + u * 0.5f;
            if (other.velocity.magnitude < 0.1)
                p = velocity + u;
            line.point = new Vector2(p.x, p.z);
            //line.direction = new Vector2(u.normalized.x, u.normalized.z);
            orcaLines.Add(line);
            test = line;
            //if (asd)
            //{
            //Debug.DrawLine(point, u, Color.green);
            //Debug.DrawRay(velocity + u * 0.5f, orca.normal, Color.green);
            //Debug.DrawLine(transform.position, u, Color.magenta);
            //}
            //newVelocity = newVelocity + u * 0.5f;
            //newVelocity = newVelocity.normalized * speed;
            //cc.SimpleMove(velocity);
            /*}
            else
            {
                Line line = new Line();
                if (d1 < d2)
                    line.direction = new Vector2(p1.normal.x, p1.normal.z);
                else
                    line.direction = new Vector2(p2.normal.x, p2.normal.z);
                //Vector2 a = Vector3.Dot(new Vector2(transform.position.x,transform.position.z) + newVelocity - new Vector2(apex.x, apex.z), line.direction) * line.direction;
                Vector2 a = Vector3.Dot(newVelocity - new Vector2(apex.x, apex.z), line.direction) * line.direction;
                b = velocity + new Vector3(a.x, 0, a.y);
                line.point = new Vector2(b.x, b.z);
                test = line;
                orcaLines.Add(line);
            }*/
            //else if (p1.SameSide(relVel,target.position) || p2.SameSide(relVel,target.position))
            /*else if (p1.GetSide(target.position) || p2.GetSide(target.position))
            {
                //cc.SimpleMove(velocity);
                //newVelocity = (target.position - transform.position).normalized * speed;
            }*/
        }
        

        int lineFail = LinearProgram.linearProgram2(orcaLines, speed, prefVelocity, false, ref newVelocity);
        //Debug.Log("Result: " + newVelocity);
        //Debug.Log(lineFail);

        if (lineFail < orcaLines.Count)
        {
            Debug.Log(this.name + " ERROR");
            LinearProgram.linearProgram3(orcaLines, numObstacles, lineFail, speed, ref newVelocity);
            //newVelocity = Vector2.zero;
        }

            /*
            Vector3 dir = new Vector3(test.direction.x, 0, test.direction.y);
            Vector3 po = new Vector3(test.point.x, 0, test.point.y);
            Plane plane = new Plane(dir,po);
            if (!plane.GetSide(orcaLines[0].point))
                newVelocity = (orcaLines[0].point - new Vector2(transform.position.x, transform.position.z)).normalized * speed;
            else
                newVelocity = prefVelocity;
            */

            //cc.SimpleMove(velocity);
            //Debug.DrawRay(transform.position, velocity - other.velocity,Color.black);
            //findTangents(apex, center, r);
        }

        void OnDrawGizmosSelected()
    {
        
        foreach(Line line in orcaLines)
        {
            //Debug.Log(line.direction);
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(new Vector3(line.point.x, 0, line.point.y), 0.1f);
            Gizmos.DrawRay(new Vector3(line.point.x, 0, line.point.y), new Vector3(line.direction.x, 0, line.direction.y));
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(new Vector3(0, 0, 0), new Vector3(newVelocity.x, 0, newVelocity.y));
        /*
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(center, r);
        Gizmos.DrawSphere(apex, r/timeInv);
        Gizmos.color = Color.black;
        Gizmos.DrawLine(tp1, tp3);
        Gizmos.DrawLine(tp2, tp4);
        Gizmos.color = Color.red;
        Gizmos.DrawRay(tp1, p1.normal);
        Gizmos.DrawRay(tp2, p2.normal);
        Gizmos.color = Color.green;
        Gizmos.DrawRay(Vector3.zero, velocity - agents[1].velocity);
        Gizmos.DrawSphere(velocity - agents[1].velocity, 0.1f);
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(u, 0.1f);
        */
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(center, 0.1f);
        Gizmos.DrawSphere(apex, 0.1f);
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(tp1, 0.1f);
        Gizmos.DrawSphere(tp3, 0.1f);
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(tp2, 0.1f);
        Gizmos.DrawSphere(tp4, 0.1f);
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(new Vector3(test.point.x,0, test.point.y), 0.2f);
        Gizmos.DrawRay(new Vector3(test.point.x, 0, test.point.y), new Vector3(test.direction.x, 0, test.direction.y));

        //Gizmos.DrawLine(apex, tp1);
        //Gizmos.DrawLine(apex, tp2);
        //Gizmos.DrawLine(Vector3.zero, tp1);
        //Gizmos.DrawLine(Vector3.zero, tp2);
        //Gizmos.DrawRay(apex, p1.normal);
        //Gizmos.DrawRay(apex, p2.normal);
        //Gizmos.DrawSphere(transform.position + new Vector3(newVelocity.x, 0, newVelocity.y), 0.1f);
        //Gizmos.DrawRay(transform.position, new Vector3(prefVelocity.x, 0, prefVelocity.y));
        //Gizmos.DrawLine(apex, apex+new Vector3(0,0,1));
        //Gizmos.DrawRay(apex, Vector3.Cross(center + new Vector3(0, 0, r), Vector3.down));
        //Gizmos.DrawRay(apex, Vector3.Cross(center + new Vector3(0, 0, -r), Vector3.up));
        //Vector3 v = center + new Vector3(0, 0, r) - apex;
        //Gizmos.DrawRay(apex, new Vector3(-v.z, 0, v.x).normalized*-1);
        //v = center + new Vector3(0, 0, -r) - apex;
        //Gizmos.DrawRay(apex, new Vector3(-v.z, 0, v.x).normalized);
    }

    void findTangents(Vector3 a, Vector3 b, float r, ref Vector3 tp1, ref Vector3 tp2)
    {
        Vector3 v = b - a;
        float d = v.magnitude;
        double alpha = Mathf.Asin(r / d);
        double gamma = Mathf.PI * 0.5 - alpha;
        double offset = Mathf.Atan2(a.z-b.z, a.x-b.x);
        double theta1 = offset - gamma;
        double theta2 = offset + gamma;
        tp1 = new Vector3(b.x + r * Mathf.Cos((float)theta2), 0, b.z + r * Mathf.Sin((float)theta2));
        tp2 = new Vector3(b.x + r * Mathf.Cos((float)theta1), 0, b.z + r * Mathf.Sin((float)theta1));
        /*
        Debug.Log(alpha*Mathf.Rad2Deg);
        Vector3 line = b;
        Quaternion q = new Quaternion();
        q.SetEulerRotation(0, alpha, 0);
        Debug.DrawLine(a, q*line, Color.cyan);
        q.SetEulerRotation(0, -alpha, 0);
        Debug.DrawLine(a, q * line, Color.cyan);
        */
        /*
        Vector3 ray1 = a - b;
        Vector3 ray2 = Vector3.up;
        Vector3 axisRay = Vector3.Cross(ray1, ray2);
        Vector3 tp1 = axisRay.normalized * r + b;
        Vector3 tp2 = -axisRay.normalized * r + b;
        Debug.DrawLine(a, tp1, Color.cyan);
        Debug.DrawLine(a, tp2, Color.cyan);
        */
    }

    Vector2 vec3tovec2(Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }
    Vector2 vec2tovec3(Vector2 v)
    {
        return new Vector3(v.x, 0, v.y);
    }
}