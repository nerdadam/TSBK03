﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class Movement : MonoBehaviour {

    public bool Astar;
    public float goalDistance = 1;
    public bool useForce;

    private MousePosition mp;
    private CharacterController cc;
    private Agent myAgent;
    private Seeker seeker;
    private Path path;
    private int currentWaypoint = 0;
    private float waypointDistance;
    public Vector3 target;
    private bool updatePath = false;
    private AstarPath ap;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        mp = FindObjectOfType<MousePosition>();
        cc = GetComponent<CharacterController>();
        myAgent = GetComponent<Agent>();
        seeker = GetComponent<Seeker>();
        ap = FindObjectOfType<AstarPath>();
        rb = GetComponent<Rigidbody>();
        if (Astar)
        {
            AstarPath.OnGraphsUpdated += OnGraphsUpdated;
            //target = transform.position;
            //getPathRepeat();
        } else
        {
            target = myAgent.target;
        }
    }

    void Update()
    {
        if (myAgent.selected)
        {

            if (Input.GetMouseButtonDown(1))
            {
                target = mp.getMousePosition();
                Debug.Log(target);
                myAgent.target = new Vector3(target.x, 0, target.z);
                //myAgent.newVelocity = Vector3.zero;
                myAgent.goalReached = false;
                walk();
                if (Astar)
                    seeker.StartPath(transform.position, new Vector3(target.x, 0, target.z), OnPathComplete);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                target = transform.position;
                path = null;
                stop();
            }
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (ap)
        {
            /*
            if ((new Vector3(target.x, 0, target.z) - transform.position).magnitude >= goalDistance + myAgent.radius * 4 + 0.1 && !useAstar() && Astar)
            {
                Astar = false;
                myAgent.target = target;
            }

            if (myAgent.selected)
            {

                if (Input.GetMouseButtonDown(1))
                {
                    target = mp.getMousePosition();
                    myAgent.target = new Vector3(target.x, 0, target.z);
                    //myAgent.newVelocity = Vector3.zero;
                    myAgent.goalReached = false;
                    walk();
                }
            }

            if (((new Vector3(target.x, 0, target.z) - transform.position).magnitude < goalDistance + myAgent.radius * 4 || useAstar()) && !Astar)
            {
                Astar = true;
                //myAgent.newVelocity = Vector3.zero;
                myAgent.goalReached = false;
                walk();
                seeker.StartPath(transform.position, new Vector3(target.x, 0, target.z), OnPathComplete);
            }
            */
            if (useAstar() && !Astar && (new Vector3(target.x, 0, target.z) - transform.position).magnitude > goalDistance)
            {
                Astar = true;
                //myAgent.newVelocity = Vector3.zero;
                myAgent.goalReached = false;
                walk();
                seeker.StartPath(transform.position, new Vector3(target.x, 0, target.z), OnPathComplete);
            } else if (!useAstar() && Astar)
            {
                Astar = false;
                myAgent.target = target;
            }
        }

        if (!Astar)
        {
            if ((new Vector3(target.x, 0, target.z) - transform.position).magnitude < goalDistance)
            {
                myAgent.goalReached = true;
                stop();
            }
            else
                updatePosition();
            return;
        }

         if (path == null)
        {
            //We have no path to move after yet
            return;
        }
        if (currentWaypoint >= path.vectorPath.Count || (new Vector3(target.x, 0, target.z) - transform.position).magnitude < goalDistance)
        {
            //Debug.Log("End Of Path Reached");
            myAgent.goalReached = true;
            //updatePath = false;
            stop();
            path = null;
            return;
        }
        //Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        dir = transform.position + dir;
        myAgent.target = new Vector3(dir.x, 0, dir.z);
        //myAgent.calcVelocity();
        updatePosition();
        //transform.position += new Vector3(dir.x, 0, dir.z) * Time.deltaTime;
        //cc.SimpleMove(dir);
        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (currentWaypoint + 1 == path.vectorPath.Count)
            waypointDistance = goalDistance;
        else
            waypointDistance = myAgent.radius*2;
        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < waypointDistance)
        {
            currentWaypoint++;
            return;
        }
    }

    public void OnPathComplete(Path p)
    {
        //Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
        if (!p.error)
        {
            path = p;
            //Reset the waypoint counter
            currentWaypoint = 0;
            //AstarPath.OnGraphsUpdated += OnGraphsUpdated;
        }
    }

    public void OnGraphsUpdated(AstarPath script)
    {
        getPath();
    }

    void getPath()
    {
        //if (!updatePath)
        //    return;
        if (seeker.IsDone())
            seeker.StartPath(transform.position, new Vector3(target.x, 0, target.z), OnPathComplete);
    }

    void getPathRepeat()
    {
        if (seeker.IsDone())
            seeker.StartPath(transform.position, new Vector3(target.x, 0, target.z), OnPathComplete);
        Invoke("getPathRepeat", Random.Range(1, 3));
    }

    void stop()
    {
        if (gameObject.layer != 8 && ap)
        {
            gameObject.layer = 8; //Obstacle
            AstarPath.active.UpdateGraphs(gameObject.GetComponent<Collider>().bounds);
        }
        if (useForce)
        {
            rb.velocity = Vector3.zero;
            rb.mass = 10000;
        }
    }

    void walk()
    {
        if (gameObject.layer != 0 && ap)
        {
            gameObject.layer = 0; //Default
            AstarPath.active.UpdateGraphs(gameObject.GetComponent<Collider>().bounds);
        }
        if (useForce)
        {
            rb.mass = 1;
        }
    }

    void updatePosition()
    {
        //myAgent.calcVelocity();
        Vector2 v = myAgent.newVelocity;
        if (useForce)
        {
            Vector3 w = (new Vector3(v.x, 0, v.y) - rb.velocity) / Time.fixedDeltaTime;
            float maxForce = 1;
            if (w.x > maxForce)
                w.x = maxForce;
            else if (w.x < -maxForce)
                w.x = -maxForce;
            if (w.z > maxForce)
                w.z = maxForce;
            else if (w.z < -maxForce)
                w.z = -maxForce;
            if (Time.deltaTime > 0)
                rb.AddForce(w.x, 0, w.z);
            myAgent.velocity = rb.velocity;
            myAgent.newVelocity = rb.velocity;
        }
        else
            transform.position += new Vector3(v.x, 0, v.y) * Time.fixedDeltaTime;
    }

    bool useAstar()
    {
        if (myAgent.obstacles.Count > 0)
            return true;
        foreach (Agent agent in myAgent.agents)
        {
            if (agent.velocity.magnitude == 0)
                return true;
        }
        return false;
    }
}
