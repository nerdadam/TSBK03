﻿using UnityEngine;
using System.Collections;

public class ChangeLevel : MonoBehaviour {

    public Movement prefab;
    public static int currentLevel;
    

	// Use this for initialization
	void Start () {
        Debug.Log(currentLevel);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            prefab.Astar = false;
            Application.LoadLevel("two");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            prefab.Astar = false;
            Application.LoadLevel("multiple");
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            prefab.Astar = false;
            Application.LoadLevel("sidewalk");
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            prefab.Astar = true;
            Application.LoadLevel("follow");
        }
    }

    public void loadlevel(int i)
    {
        currentLevel = i;
        if (i == 3)
            prefab.Astar = true;
        else
            prefab.Astar = false;
        Application.LoadLevel(i);
    }
}
