﻿using UnityEngine;
using System.Collections;

public class Line {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public Vector2 point;
    public Vector2 direction;

    public void clearLine()
    {
        point = Vector2.zero;
        direction = Vector2.zero;
    }
}
