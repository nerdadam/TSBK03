﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public List<Agent> agents = new List<Agent>();

	// Use this for initialization
	void Start () {
        //Time.timeScale = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
            Time.timeScale = 1 - Time.timeScale;
        if (Input.GetKeyDown(KeyCode.R))
            Application.LoadLevel(0);
        
    }

    void FixedUpdate()
    {
        foreach (Agent agent in agents)
        {
            if (agent)
                agent.calcVelocity();
        }
        foreach (Agent agent in agents)
        {
            if (agent)
                agent.velocity = new Vector3(agent.newVelocity.x,0,agent.newVelocity.y);
        }
    }
}
