﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EditAgent : MonoBehaviour {

    public Agent prefab;
    public Spawner spawner;
    public Toggle forceToggle;

    private string speed, tau, goalDistance, radius, spawnTime;
    private int xStart, yStart, i;
    private Movement ms;
    private bool force;
    private Rigidbody rb;
    private SphereCollider sc;

    // Use this for initialization
    void Start () {
        ms = prefab.GetComponent<Movement>();
        rb = prefab.GetComponent<Rigidbody>();
        sc = prefab.GetComponent<SphereCollider>();
        i = 4;
        if (spawner)
            i++;
        xStart = 20;
        yStart = Screen.height - 30*i;
        speed = prefab.speed.ToString();
        tau = prefab.timeInv.ToString();
        goalDistance = ms.goalDistance.ToString();
        force = ms.useForce;
        radius = prefab.radius.ToString();
        if (spawner)
            spawnTime = spawner.spawnTime.ToString();
        //prefab.transform.localScale = new Vector3(2 * prefab.radius, 0, 2 * prefab.radius);
        forceToggle.isOn = ms.useForce;
	}
	
	// Update is called once per frame
	void Update () {
        ms.useForce = forceToggle.isOn;
        rb.isKinematic = !forceToggle.isOn;
    }

    void OnGUI()
    {
        i = 0;
        if (spawner)
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(xStart, yStart, 50, 20), "Rate: ");
            GUI.color = Color.white;
            spawnTime = GUI.TextField(new Rect(xStart + 55, yStart, 30, 20), spawnTime);
            float time = float.Parse(spawnTime);
            spawner.spawnTime = time;
            foreach (Spawner s in FindObjectsOfType<Spawner>())
                s.spawnTime = time;
            i++;
        }
        GUI.color = Color.black;
        GUI.Label(new Rect(xStart, yStart + 30 * i, 50, 20), "Speed: ");
        GUI.color = Color.white;
        speed = GUI.TextField(new Rect(xStart+55, yStart + 30 * i, 30, 20), speed);
        prefab.speed = float.Parse(speed);
        i++;
        GUI.color = Color.black;
        GUI.Label(new Rect(xStart, yStart+ 30 * i, 50, 20), "Tau: ");
        GUI.color = Color.white;
        tau = GUI.TextField(new Rect(xStart + 55, yStart+ 30 * i, 30, 20), tau);
        prefab.timeInv = int.Parse(tau);
        i++;
        GUI.color = Color.black;
        GUI.Label(new Rect(xStart, yStart + 30 * i, 50, 20), "Goal: ");
        GUI.color = Color.white;
        goalDistance = GUI.TextField(new Rect(xStart + 55, yStart + 30 * i, 30, 20), goalDistance);
        ms.goalDistance = float.Parse(goalDistance);
        i++;
        GUI.color = Color.black;
        GUI.Label(new Rect(xStart, yStart + 30 * i, 50, 20), "Radius: ");
        GUI.color = Color.white;
        radius = GUI.TextField(new Rect(xStart + 55, yStart + 30 * i, 30, 20), radius);
        prefab.radius = float.Parse(radius);
        sc.radius = float.Parse(radius);
        //prefab.transform.localScale = new Vector3(2 * float.Parse(radius), 1, 2 * float.Parse(radius));
        //i++;
        //GUI.color = Color.black;
        //GUI.Label(new Rect(xStart, yStart + 30 * i, 50, 20), "Force: ");
        //ms.useForce = GUI.Toggle(new Rect(xStart + 55, yStart + 25 * i, 30, 20), ms.useForce,"");
        //rb.isKinematic = !ms.useForce;
    }
}
