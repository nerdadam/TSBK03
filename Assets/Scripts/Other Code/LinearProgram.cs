﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LinearProgram : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    static bool linearProgram1(IList<Line> lines, int lineNo, float radius, Vector2 optVelocity, bool directionOpt, ref Vector2 result)
    {
        float dotProduct = Vector2.Dot(lines[lineNo].point,lines[lineNo].direction);
        float discriminant = RVOMath.sqr(dotProduct) + RVOMath.sqr(radius) - RVOMath.absSq(lines[lineNo].point);

        if (discriminant < 0.0f)
        {
            /* Max speed circle fully invalidates line lineNo. */
            return false;
        }

        float sqrtDiscriminant = RVOMath.sqrt(discriminant);
        float tLeft = -dotProduct - sqrtDiscriminant;
        float tRight = -dotProduct + sqrtDiscriminant;

        for (int i = 0; i < lineNo; ++i)
        {
            float denominator = RVOMath.det(lines[lineNo].direction, lines[i].direction);
            float numerator = RVOMath.det(lines[i].direction, lines[lineNo].point - lines[i].point);

            if (RVOMath.fabs(denominator) <= RVOMath.RVO_EPSILON)
            {
                /* Lines lineNo and i are (almost) parallel. */
                if (numerator < 0.0f)
                {
                    return false;
                }
                else
                {
                    continue;
                }
            }

            float t = numerator / denominator;

            if (denominator >= 0.0f)
            {
                /* Line i bounds line lineNo on the right. */
                tRight = Mathf.Min(tRight, t);
            }
            else
            {
                /* Line i bounds line lineNo on the left. */
                tLeft = Mathf.Max(tLeft, t);
            }

            if (tLeft > tRight)
            {
                return false;
            }
        }

        if (directionOpt)
        {
            /* Optimize direction. */
            if (Vector2.Dot(optVelocity,lines[lineNo].direction) > 0.0f)
            {
                /* Take right extreme. */
                result = lines[lineNo].point + tRight * lines[lineNo].direction;
            }
            else
            {
                /* Take left extreme. */
                result = lines[lineNo].point + tLeft * lines[lineNo].direction;
            }
        }
        else
        {
            /* Optimize closest point. */
            float t = Vector2.Dot(lines[lineNo].direction,(optVelocity - lines[lineNo].point));
            if (t < tLeft)
            {
                result = lines[lineNo].point + tLeft * lines[lineNo].direction;
            }
            else if (t > tRight)
            {
                result = lines[lineNo].point + tRight * lines[lineNo].direction;
            }
            else
            {
                result = lines[lineNo].point + t * lines[lineNo].direction;
            }
            //Debug.Log("tleft: " + tLeft + " tright: " + tRight + " t: " + t);
            //Debug.Log("Point: " + lines[lineNo].point + " Dir: " + lines[lineNo].direction);
            //Debug.Log(result);
        }

        return true;
    }

    public static int linearProgram2(IList<Line> lines, float radius, Vector2 optVelocity, bool directionOpt, ref Vector2 result)
    {
        if (directionOpt)
        {
            /*
             * Optimize direction. Note that the optimization velocity is of unit
             * length in this case.
             */
            result = optVelocity * radius;
        }
        else if (RVOMath.absSq(optVelocity) > RVOMath.sqr(radius))
        {
            /* Optimize closest point and outside circle. */
            result = RVOMath.normalize(optVelocity) * radius;
        }
        else
        {
            /* Optimize closest point and inside circle. */
            result = optVelocity;
        }
        //Debug.Log("Before: " + result);
        for (int i = 0; i < lines.Count; ++i)
        {
            //Debug.Log(lines[i].point + " " + lines[i].direction);
            if (RVOMath.det(lines[i].direction, lines[i].point - result) > 0.0f)
            {
                
                /* Result does not satisfy constraint i. Compute new optimal result. */
                Vector2 tempResult = result;
                if (!linearProgram1(lines, i, radius, optVelocity, directionOpt, ref result))
                {
                    result = tempResult;
                    return i;
                }
            }
        }
        //Debug.Log("After: " + result);
        return lines.Count;
    }

    public static void linearProgram3(IList<Line> lines, int numObstLines, int beginLine, float radius, ref Vector2 result)
    {
        float distance = 0.0f;

        for (int i = beginLine; i < lines.Count; ++i)
        {
            if (RVOMath.det(lines[i].direction, lines[i].point - result) > distance)
            {
                /* Result does not satisfy constraint of line i. */
                //std::vector<Line> projLines(lines.begin(), lines.begin() + numObstLines);
                IList<Line> projLines = new List<Line>();
                for (int ii = 0; ii < numObstLines; ++ii)
                {
                    projLines.Add(lines[ii]);
                }

                for (int j = numObstLines; j < i; ++j)
                {
                    Line line = new Line();

                    float determinant = RVOMath.det(lines[i].direction, lines[j].direction);

                    if (RVOMath.fabs(determinant) <= RVOMath.RVO_EPSILON)
                    {
                        /* Line i and line j are parallel. */
                        if (Vector2.Dot(lines[i].direction,lines[j].direction) > 0.0f)
                        {
                            /* Line i and line j point in the same direction. */
                            continue;
                        }
                        else
                        {
                            /* Line i and line j point in opposite direction. */
                            line.point = 0.5f * (lines[i].point + lines[j].point);
                        }
                    }
                    else
                    {
                        line.point = lines[i].point + (RVOMath.det(lines[j].direction, lines[i].point - lines[j].point) / determinant) * lines[i].direction;
                    }

                    line.direction = RVOMath.normalize(lines[j].direction - lines[i].direction);
                    projLines.Add(line);
                }

                Vector2 tempResult = result;
                if (linearProgram2(projLines, radius, new Vector2(-lines[i].direction.y, lines[i].direction.x), true, ref result) < projLines.Count)
                {
                    /* This should in principle not happen.  The result is by definition
                     * already in the feasible region of this linear program. If it fails,
                     * it is due to small floating point error, and the current result is
                     * kept.
                     */
                    result = tempResult;
                }

                distance = RVOMath.det(lines[i].direction, lines[i].point - result);
            }
        }
    }
    }
