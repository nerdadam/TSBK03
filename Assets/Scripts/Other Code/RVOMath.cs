﻿using UnityEngine;
using System.Collections;

public class RVOMath : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static float absSq(Vector2 v)
    {
        //return v * v;
        return Vector2.SqrMagnitude(v);
    }
    public static Vector2 normalize(Vector2 v)
    {
        return v.normalized;
    }

    internal const float RVO_EPSILON = 0.00001f;

    internal static float sqrt(float a)
    {
        return (float)Mathf.Sqrt(a);
    }
    internal static float fabs(float a)
    {
        return Mathf.Abs(a);
    }
    internal static float distSqPointLineSegment(Vector2 a, Vector2 b, Vector2 c)
    {
        float r = (Vector2.Dot((c - a),(b - a))) / absSq(b - a);

        if (r < 0.0f)
        {
            return absSq(c - a);
        }
        else if (r > 1.0f)
        {
            return absSq(c - b);
        }
        else
        {
            return absSq(c - (a + r * (b - a)));
        }
    }
    internal static float sqr(float p)
    {
        return p * p;
    }
    internal static float det(Vector2 v1, Vector2 v2)
    {
        return v1.x * v2.y - v1.y * v2.x;
    }
    internal static float abs(Vector2 v)
    {
        return (float)Mathf.Sqrt(absSq(v));
    }
    internal static float leftOf(Vector2 a, Vector2 b, Vector2 c)
    {
        return det(a - c, b - a);
    }
}
