﻿using UnityEngine;
using System.Collections;

public class SpawnAgent : MonoBehaviour {

    public Agent prefab;
    public Vector3 target;
    public bool astar;
    public Color myColor;

	// Use this for initialization
	void Start () {
        Agent agent = (Agent)Instantiate(prefab, transform.position, Quaternion.identity);
        agent.target = target;
        agent.GetComponent<Movement>().target = target;
        FindObjectOfType<GameManager>().agents.Add(agent);
        agent.GetComponent<MeshRenderer>().material.color = myColor;
        agent.GetComponent<Movement>().Astar = astar;
        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
