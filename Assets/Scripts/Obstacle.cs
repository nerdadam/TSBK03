﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Obstacle : MonoBehaviour {

    public List<Plane> planes = new List<Plane>();
    public Vector3 point1, point2, point3, point4;

    private Collider col;

    // Use this for initialization
    void Start () {
        col = GetComponent<Collider>();
        planes.Add(new Plane());
        planes.Add(new Plane());
        planes.Add(new Plane());
        planes.Add(new Plane());
    }
	
	// Update is called once per frame
	void Update () {
        Quaternion q = transform.rotation;
        point1 = q * transform.localScale / 2 + col.bounds.center;
        point1.y = 0;
        point3 = q * -transform.localScale / 2 + col.bounds.center;
        point3.y = 0;
        q *= Quaternion.Euler(new Vector3(0, 90, 0));
        point2 = q * transform.localScale / 2 + col.bounds.center;
        point2.y = 0;
        point4 = q * -transform.localScale / 2 + col.bounds.center;
        point4.y = 0;
        planes[0] = new Plane(Vector3.Cross(Vector3.up,(point1 - point2).normalized), point1);
        planes[1] = new Plane(Vector3.Cross(Vector3.up, (point2 -point3).normalized), point2);
        planes[2] = new Plane(Vector3.Cross(Vector3.up, (point3 - point4).normalized), point3);
        planes[3] = new Plane(Vector3.Cross(Vector3.up, (point4 - point1).normalized), point4);
        /*
        Debug.DrawRay(point1, planes[0].normal, Color.red);
        Debug.DrawRay(point2, planes[1].normal, Color.red);
        Debug.DrawRay(point3, planes[2].normal, Color.red);
        Debug.DrawRay(point4, planes[3].normal, Color.red);
        */
    }

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(point1, 0.2f);
        Gizmos.DrawSphere(point2, 0.2f);
        Gizmos.DrawSphere(point3, 0.2f);
        Gizmos.DrawSphere(point4, 0.2f);
    }
}
