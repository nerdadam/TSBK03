﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NearbyObjects : MonoBehaviour
{
    public List<Agent> agents;
    public List<Obstacle> obstacles;

    private Agent myAgent;
    private SphereCollider cc;

    // Use this for initialization
    void Start()
    {
        myAgent = GetComponentInParent<Agent>();
        cc = GetComponent<SphereCollider>();
        cc.radius = (myAgent.speed * myAgent.timeInv + myAgent.radius) * 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (myAgent && agents.Count > 0)
        {
            foreach (Agent a in agents)
                myAgent.agents.Add(a);
            agents.Clear();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        Obstacle obstacle = other.GetComponent<Obstacle>();
        if (agent && other.transform.name.CompareTo("RangeCollider") == 0)
        {
            if (myAgent)
                myAgent.agents.Add(agent);
            else
                agents.Add(agent);
        }
        if (obstacle)
            myAgent.obstacles.Add(obstacle);
    }

    void OnTriggerExit(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        Obstacle obstacle = other.GetComponent<Obstacle>();
        if (agent && other.transform.name.CompareTo("RangeCollider") == 0)
            myAgent.agents.Remove(agent);
        if (obstacle)
            myAgent.obstacles.Remove(obstacle);
    }
}