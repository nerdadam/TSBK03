﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Selected : MonoBehaviour {

    private MousePosition mp;
    private Rect box;
    private List<Agent> agents;
    public LineRenderer lr;
    private LineRenderer top, bot, left, right;
    private Vector3 start, end;

	// Use this for initialization
	void Start () {
        mp = FindObjectOfType<MousePosition>();
        agents = FindObjectOfType<GameManager>().agents;
        top = (LineRenderer)Instantiate(lr, transform.position, Quaternion.identity);
        top.SetColors(Color.green, Color.green);
        bot = (LineRenderer)Instantiate(lr, transform.position, Quaternion.identity);
        bot.SetColors(Color.green, Color.green);
        left = (LineRenderer)Instantiate(lr, transform.position, Quaternion.identity);
        left.SetColors(Color.green, Color.green);
        right = (LineRenderer)Instantiate(lr, transform.position, Quaternion.identity);
        right.SetColors(Color.green, Color.green);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePosition = mp.getMousePosition();
            box.x = mousePosition.x;
            box.y = mousePosition.z;
            top.enabled = true;
            bot.enabled = true;
            left.enabled = true;
            right.enabled = true;
            start = mp.getMousePosition();
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePosition = mp.getMousePosition();
            box.width = -box.x + mousePosition.x;
            box.height = -box.y + mousePosition.z;
            top.SetPosition(0, new Vector3(box.x, 0, box.y));
            top.SetPosition(1, new Vector3(box.x + box.width, 0, box.y));
            right.SetPosition(0, new Vector3(box.x + box.width, 0, box.y));
            right.SetPosition(1, new Vector3(box.x + box.width, 0, box.y + box.height));
            bot.SetPosition(0, new Vector3(box.x + box.width, 0, box.y + box.height));
            bot.SetPosition(1, new Vector3(box.x, 0, box.y + box.height));
            left.SetPosition(0, new Vector3(box.x, 0, box.y + box.height));
            left.SetPosition(1, new Vector3(box.x, 0, box.y));
        }
        if (Input.GetMouseButtonUp(0))
        {
            end = mp.getMousePosition();
            foreach (Agent a in agents)
            {
                if (insideBox(a.transform.position))
                {
                    a.selected = true;
                    a.GetComponent<MeshRenderer>().material.color = Color.red;
                }
                else
                {
                    a.selected = false;
                    a.GetComponent<MeshRenderer>().material.color = Color.black;
                }
                
            }
            top.enabled = false;
            bot.enabled = false;
            left.enabled = false;
            right.enabled = false;
            box.width = 0;
            box.height = 0;
        }
    }

    bool insideBox(Vector3 v)
    {
        float xMax = Mathf.Max(start.x, end.x);
        float xMin = Mathf.Min(start.x, end.x);
        float zMax = Mathf.Max(start.z, end.z);
        float zMin = Mathf.Min(start.z, end.z);
        return (v.x > xMin && v.x < xMax && v.z > zMin && v.z < zMax);
    }
}
