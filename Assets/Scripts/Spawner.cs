﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    public Agent prefab;
    public Transform spawnPoint;
    public Vector3 dir;
    public float spawnTime;
    public Color myColor;

    private GameManager gm;

	// Use this for initialization
	void Start () {
        gm = FindObjectOfType<GameManager>();
        spawnAgent();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void spawnAgent()
    {
        float r = (4-Random.Range(0, 9))*0.25f;
        Vector3 sp = new Vector3(spawnPoint.position.x+r*dir.z, spawnPoint.position.y, spawnPoint.position.z + r*dir.x);
        Agent agent = (Agent)Instantiate(prefab, sp, Quaternion.identity);
        agent.target = transform.position + dir.normalized * 100;
        agent.GetComponent<MeshRenderer>().material.color = myColor;
        agent.GetComponent<Movement>().target = transform.position + dir.normalized * 100;
        Destroy(agent.gameObject,60);
        gm.agents.Add(agent);
        Invoke("spawnAgent", Random.Range(1,spawnTime));
    }
}
