The code for this project can be found Under Assets/Scripts
The most interesting script is the Agent.cs script. It contains the algorithm for the collision avoidance.
Also worth checking out is the Movement script which handles the movement of agents.
Other scripts are mostly general scripts that are needed.
Under Other Code is the code for the Linear programming that I've used.

Under Build/Orca there are three version of the simulation for each major OS
Only windows has been tested, but it will hopefully work on others as well.
When running the simulation you can pause/resume it by pressing enter.
In order for changes to affect the agents, the level has to be restarted.